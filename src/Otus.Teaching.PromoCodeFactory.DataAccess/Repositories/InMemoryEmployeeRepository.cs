﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryEmployeeRepository : InMemoryRepository<Employee>
    {
        public InMemoryEmployeeRepository(ICollection<Employee> data) : base(data) { }

        public override Task<Employee> UpdateAsync(Guid id, Employee item)
        {
            var existingItem = Data.FirstOrDefault(x => x.Id == id);
            if (existingItem == null)
            {
                return Task.FromResult<Employee>(null);
            }

            existingItem.FirstName = item.FirstName;
            existingItem.LastName = item.LastName;
            existingItem.Email = item.Email;
            existingItem.AppliedPromocodesCount = item.AppliedPromocodesCount;
            existingItem.Roles = item.Roles;

            return Task.FromResult(existingItem);
        }
    }
}