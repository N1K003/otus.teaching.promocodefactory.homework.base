﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        public InMemoryRepository(ICollection<T> data)
        {
            Data = data;
        }

        protected ICollection<T> Data { get; set; }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> CreateAsync(T item)
        {
            Data.Add(item);

            return Task.FromResult(item);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public virtual Task<T> UpdateAsync(Guid id, T item)
        {
            var existingItem = Data.FirstOrDefault(x => x.Id == id);
            if (existingItem == null)
            {
                return Task.FromResult((T) null);
            }

            existingItem = item;

            return Task.FromResult(existingItem);
        }

        public Task DeleteAsync(Guid id)
        {
            var existingItem = Data.FirstOrDefault(x => x.Id == id);
            if (existingItem != null)
            {
                Data.Remove(existingItem);
            }

            return Task.CompletedTask;
        }
    }
}