﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Request
{
    public class UpdateEmployeeRequest
    {
        public int AppliedPromocodesCount { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Guid> Roles { get; set; } = new List<Guid>();
    }
}